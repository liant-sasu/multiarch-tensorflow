---
layout: page
title: Wheel packages
permalink: /wheels/
---

# Simple installation

You have several options to install the wheel.

    pip install --index https://gitlab.com/api/v4/projects/37601282/packages/pypi/simple tensorflow

You can specify a version (see the list of available versions [below](#list)):

    pip install --index https://gitlab.com/api/v4/projects/37601282/packages/pypi/simple tensorflow

# Add our registry to the indexes of pip

You can ofcourse add this index to yours in the .config/pip/pip.conf file:

    [global]
    extra-index-url = https://gitlab.com/api/v4/projects/37601282/packages/pypi/simple

It is a space separated list, and setting it will overide any previous defined ones.
For example debian 11 add `https://www.piwheels.org/simple` so we define our pip.conf like that:

    [global]
    extra-index-url = https://www.piwheels.org/simple https://gitlab.com/api/v4/projects/37601282/packages/pypi/simple

Then you can just do:

    pip install tensorflow==<version>

and this should work.

# List

Available versions of the package:

## Python 3.9

The tags are: `cp39` `cp39` `linux_aarch64`.
This is the version natively insalled on Debian Bullseye (see [here](https://packages.debian.org/bullseye/python3)):

- 2.9.1
- 2.8.0
- 2.7.0
- 2.6.0

## Python 3.8 

The tags are: `cp38` `cp38` `linux_aarch64`.
This is the version natively installed on Ubuntu 20.4 (see [here](https://packages.ubuntu.com/search?suite=focal&arch=arm64&keywords=python3)):

- 2.6.0
- 2.5.0
- 2.4.1
- 2.4.0
- 2.3.1
- 2.3.0

## Python 3.7

The tags are: `cp37` `cp37m` `linux_aarch64`.
This is the version natively insalled on Debian Buster (see [here](https://packages.debian.org/buster/python3)):

- 2.6.0
- 2.5.1
- 2.5.0
- 2.4.1
- 2.4.0
- 2.3.1
- 2.3.0

## Python 3.6

The tags are: `cp36` `cp36m` `linux_aarch64`.
This is the version natively insalled on Debian 18.4 (see [here](https://packages.debian.org/buster/python3)):

- 2.4.1
- 2.4.0
- 2.3.1
- 2.3.0
