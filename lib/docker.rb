# frozen_string_literal: true

require 'English'
require 'json'

# A class to manage docker cli interactions
class Docker
  def initialize
    # First ensure login to gitlab registry
    $stdout.puts "Ensure you are connected to Gitlab's registry"
    system('docker login registry.gitlab.com')
    return if $CHILD_STATUS.success?

    warn "Please make sure you setup your environement to be able to connect to Gitlab's registry"
    exit 1
  end

  # Get the manifest of a given image.
  def manifest(image_path, opts = { 'verbose' => true })
    manifest_str = `docker manifest inspect #{prepare_options(opts).join(' ')} #{image_path}`
    return JSON.parse(manifest_str) if $CHILD_STATUS.success?

    warn "Unable to get the manifest of #{image_path}, maybe it is not present on the registry."
    false
  end

  # Launch the buildx build command
  def buildx(cmd = 'build', path = '.', opts = {})
    cmd = "docker buildx #{cmd} #{prepare_options(opts).join(' ')} #{path}"
    puts "Running: #{cmd}"
    system cmd
    $CHILD_STATUS.success?
  end

  private

  def prepare_options(opts)
    opts.keys.map do |name|
      next "--#{name}" if opts[name].is_a?(TrueClass)
      next "--#{name} #{opts[name]}" unless opts[name].is_a?(Array)

      opts[name].map { |value| "--#{name} #{value}" }
    end.flatten
  end
end
