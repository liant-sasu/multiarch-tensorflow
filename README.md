# Tensorflow arm64 repository

A repository with precompiled versions of tensorflow for raspberry pi.

## Brief

I've gather the compiled eggs from [Qengineering work](https://qengineering.eu) [here](https://github.com/Qengineering/TensorFlow-Raspberry-Pi_64-bit) and added them to a pypi registry.

Please feel free to use [the issues requests](https://gitlab.com/liant-sasu/multiarch-tensorflow/-/issues)
to make any remarks about the subject of this project (hosting some eggs and Docker images for arm64 to
ease the usage and installation of tensorflow on raspberry).

## Thanks

Thank you to all the free project to make that possible.

Particularly thank you to Qengineering for giving those wheels.

# Usages

Here we whish to ease the usage of tensorflow on arm64 (mainly for our own usage on Raspberry 4).
So we make available wheels in a pypi registry, and docker images. We may add other stuff if it
is usefull (so let me know what you need or/and make PR to add the corresponding usage documentation)

## Wheels/eggs/Pypi packages

First of all we made available several pre-compiled arm64 versions of tensorflow.
Usage is as simple as adding the following index:

See [this page](/wheels) for more informations.

## Docker images

A serie of Docker images have been built for arm64 with the same versions of tensorflow.
Take a look at [this page](/docker) for more informations.