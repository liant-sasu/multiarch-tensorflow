---
layout: page
title: Docker registry
permalink: /docker/
---

# How to use those images

Project is public so no need to login to registry.gitlab.com which is the host.

To load the image just do:

    docker pull registry.gitlab.com/liant-sasu/multiarch-tensorflow:2.9.1-bullseye

Directive `FROM` inside your docker file will look like this:

    FROM registry.gitlab.com/liant-sasu/multiarch-tensorflow:2.9.1-bullseye

# Available platforms

* `linux/arm64`
* `linux/amd64`
* `linux/arm64v8`
* `linux/armv7`

# Available tags

## Debian bulleseye

* `2.9.1-bullseye`, `latest`
* `2.8.0-bullseye`
* `2.7.0-bullseye`
* `2.6.0-bullseye`
* `2.9.1-bullseye-slim`
* `2.8.0-bullseye-slim`
* `2.7.0-bullseye-slim`
* `2.6.0-bullseye-slim`

## Debian Buster

* `2.6.0-buster`
* `2.6.0-buster-slim`
* `2.5.1-buster`
* `2.5.1-buster-slim`
* `2.5.0-buster`
* `2.5.0-buster-slim`
* `2.4.1-buster`
* `2.4.1-buster-slim`
* `2.4.0-buster`
* `2.4.0-buster-slim`
* `2.3.1-buster`
* `2.3.1-buster-slim`
* `2.3.0-buster`
* `2.3.0-buster-slim`

## Ubuntu Focal

* `2.6.0-focal`
* `2.5.0-focal`
* `2.4.1-focal`
* `2.4.0-focal`
* `2.3.1-focal`

## Ubuntu Bionic

* `2.4.1-bionic`
* `2.4.0-bionic`
* `2.3.1-bionic`
* `2.3.0-bionic`
